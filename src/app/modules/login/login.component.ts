import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { InputHelper } from 'src/app/core/inputs/helpers/input.helper';
import { InputArgs } from 'src/app/core/inputs/types/input-args.type';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  ngOnInit(): void {
  }

  loginForm = new FormGroup({
    login: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })

  get login() { return this.loginForm.get('login'); }

  get password() { return this.loginForm.get('password'); }

  get passwordInputParams(): InputArgs | null {
    return this.password ? InputHelper.createInputParams(
      {
        type: 'password',
        label: 'Password',
        placeholder: 'Password',
        formGroup: this.loginForm,
        formControlName: 'password',
      },
      this.password
    ) : null
  }

  get loginInputParams(): InputArgs | null {
    return this.login ? InputHelper.createInputParams(
      {
        type: 'text',
        label: 'Login',
        placeholder: 'Login',
        formGroup: this.loginForm,
        formControlName: 'password',
      },
      this.login
    ) : null
  }

  handleLoginFormSubmit() {
    console.log('Form submitted')
    this.loginForm.markAllAsTouched();

    if (this.loginForm.invalid) {
      // TODO: validate form
      return
    }

    console.log(this.loginForm.value)
  }
}
