import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from './registration/registration.component';
import { TextInputComponent } from 'src/app/components/inputs/text-input/text-input.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    TextInputComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    LoginComponent,
  ]
})
export class LoginModule { }
