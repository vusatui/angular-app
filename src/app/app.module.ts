import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CentredContentLayoutComponent } from './pages/layout/centred-content-layout/centred-content-layout.component';
import { MainLayoutComponent } from './pages/layout/main-layout/main-layout.component';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginModule } from './modules/login/login.module';

@NgModule({
  declarations: [
    AppComponent,
    CentredContentLayoutComponent,
    MainLayoutComponent,
    LoginPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    LoginModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
