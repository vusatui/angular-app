import { AbstractControl, FormGroup } from "@angular/forms";
import { InputArgs } from "../types/input-args.type";

export const InputHelper = {
  createInputParams(inputParams: InputArgs, abstractControl: AbstractControl): InputArgs {
    const errors = [...inputParams.errors ? inputParams.errors : []]

    if (abstractControl?.invalid && (abstractControl?.dirty || abstractControl?.touched)) {
      errors.push('Field is required') // TODO: errors messages generation
    }

    return {
      ...inputParams,
      errors
    }
  }
}
