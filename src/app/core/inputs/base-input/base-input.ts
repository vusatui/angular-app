import { Component, OnInit, Input } from '@angular/core';
import { InputArgs } from '../types/input-args.type';

@Component({
  template: ``
})
export class BaseInput implements OnInit {
  @Input() args!: InputArgs

  get lastError(): string | null {
    const errorMessage = this.args.errors?.slice().pop()

    if (!errorMessage) {
      return null
    }

    return errorMessage
  }

  ngOnInit(): void {
    console.log(this.args)
  }
}
