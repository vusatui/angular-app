import { FormGroup } from '@angular/forms'

export type InputArgs = {
  label: string
  type: string
  placeholder: string
  formGroup: FormGroup
  formControlName: string
  errors?: Array<string>
}
