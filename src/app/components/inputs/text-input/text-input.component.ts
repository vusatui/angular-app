import { Component, OnInit, Input } from '@angular/core';
import { BaseInput } from 'src/app/core/inputs/base-input/base-input';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent extends BaseInput {

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
