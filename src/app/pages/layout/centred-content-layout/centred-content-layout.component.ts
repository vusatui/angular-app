import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-centred-content-layout',
  templateUrl: './centred-content-layout.component.html',
  styleUrls: ['./centred-content-layout.component.scss'],
  host: {
    class: 'flex flex-col items-center justify-center flex-1'
  }
})
export class CentredContentLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
